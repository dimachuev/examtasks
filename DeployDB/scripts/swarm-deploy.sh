#!/usr/bin/env bash
DOCKER_COMPOSE=$1
STACK=$2

docker stack deploy -c $DOCKER_COMPOSE $STACK --with-registry-auth --resolve-image "changed"
