#!/bin/env bash

URL=$1 # аргумет для ввода адресса веб страницы для проверки
TEXT="Про важное" # Проверочная строка

TEST=$(curl --connect-timeout 5 -Is $URL | head -1 | awk '{ print $2 }') # Определение кода доступности

if [[ -z $TEST ]]; then
echo "Error $URL - не доступен"
exit 1
fi

if (( $TEST < 400 )); then
if (( $(curl -Ls $URL | grep -ic "$TEXT") > 0 )); then # Проверка содержимого на наличие условной строки.
  echo "ОК"
  else echo "NO"
fi
else echo "Error $TEST" # Вывод кода ошибки
fi
